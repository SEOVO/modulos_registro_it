from odoo import models, fields, api
class Ticket(models.Model):
    _inherit = 'helpdesk.ticket'
    modulos  = fields.One2many('modules.registro.it','ticket_id')

class ModulosIT(models.Model):
    _name = 'modules.registro.it'
    name  = fields.Char(string="Nombre")
    descripcion = fields.Char()
    ticket_id  = fields.Many2one('helpdesk.ticket')
