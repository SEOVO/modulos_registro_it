# -*- encoding: utf-8 -*-
{
    'name': 'Registro de modulos en los Tickets',
    'category': '',
    'author': 'JESUS',
    'depends': ['helpdesk_timesheet'],
    'version': '1.0',
    'description':"""
     Registro de modulos en los Tickets
    """,
    'auto_install': False,
    'demo': [],
    'data': [
        'security/ir.model.access.csv',
        'views/views.xml',
        ],
    'installable': True
}